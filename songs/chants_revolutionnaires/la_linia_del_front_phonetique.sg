\selectlanguage{french}
\songcolumns{2}
%\OldSongColumns{3}
\beginsong{La Línia del Front}
[by={Chorale des Alouettes}]



\MultiwordChords  %http:;//songs.sourceforge.net/songsdoc/songs.html#sec5.3.0.3

{
	%\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Light}]{Noto Sans Display}
	%	\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Light}]{Noto Sans}
	\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Light}]{Noto Serif Display}
	%	\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Light}]{Noto Serif}
	
	%	\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Thin}]{Noto Sans Display}
	%	\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Thin}]{Noto Sans}
	%	\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Thin}]{Noto Serif Display}
	%	\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Thin}]{Noto Serif}
	%	\newfontfamily{\ipafont}[Ligatures=TeX,UprightFont={* Light}]{DejaVu Serif}
	\newcommand{\useipafont}[1]{{\large\ipafont #1}}
	\newcommand{\wordipa}[1]{\useipafont{\textbackslash#1\textbackslash}}
	\newcommand{\ipa}[1]{[\useipafont{#1}]}
	\newcommand{\textipa}[1]{\useipafont{#1}}
	\newcommand{\gr}[1]{\underline{\textbf{#1}}}
	
	\newcommand{\sfLong}[1]{\underline{\textbf{#1}}}
	\newcommand{\sfDouble}[1]{\textcolor{gray!90!white}{\textit{#1}}}
	\newcommand{\sfV}{{\normalsize\textcolor{gray!90!white}{|}}}
	\newcommand{\sfH}{{\footnotesize\textcolor{gray!90!white}{\_~}}}
	
	
	\begin{verse}
		Ai Mare, aneu a missa
		Que jo faré el dinar
		Quan hagueu tornat de missa
		La casa buida serà
		\textnote{
			\useipafont{/Aj / ma.ɾə̆ / a.ne.e.w̆ / a / ˈmiː.sa/} \newline
			\useipafont{/Ke / d͡ʒo / fa.ˈɾe / əl / di.naɾː/} \newline
			\useipafont{/Kwan / a.ɡəw / tuɾ.nat̆ / də / ˈmiː.sa/} \newline
			\useipafont{/La / ka.za / buj.da / səˈɾa/}
		}
		
		\textnote{
		\textit{\small
		Oh maman, va à la messe\newline
		Que je ferai le déjeuner\newline
		Quand tu sera revenu de la messe \newline
		La maison sera vide
		}
		}
		
		%		S\sfLong{u} frat\sfLong{e}ll\sfLong{i} pugn\sfLong{a}mo\sfDouble{-o} da f\sfLong{o}\sfDouble{-o-}rti
		%		C\sfLong{on}troi v\sfLong{i}l\sfLong{i} tir\sfLong{a}nni\sfDouble{-i} borgh\sfLong{e}\sfDouble{-e-}si
		%		Ma co\sfV me fe\sfV ce\sfDouble{-e} Case\sfV rio e comp\sfLong{a}\sfDouble{-a-}gni
		%		Ch\sfLong{e} la m\sfLong{o}rte\sfDouble{-e} l’andi\sfLong{e}de a incontrà.
	\end{verse}

	\begin{chorus}
		La La La La .. x2
	\end{chorus}
	
	
	\begin{verse}
		No em busqueu per rius i planes
		Busqueu-me on el sol es pon
		Que som dalt d'una carreta
		Camí a la línia del front
		\textnote{
			\useipafont{/No / ə̆m / bus.kəw / pəɾ / ɾiws / i / ˈplaː.nəs/}\newline
			\useipafont{/Bus.kəw.mə̆ / ɔn / əl / sol / əs / pon/}\newline
			\useipafont{/Kə / sum / dal / d'u.na / ka.ˈrɛ.ta/}\newline
			\useipafont{/Ka.ˌmĭ / a / la / ˈli.ni.a / dəl / fɾont/}
		}
		\textnote{
		\textit{\small
		Ne me cherche pas dans les rivières et les plaines    \newline
		Trouve-moi là où le soleil se couche    \newline
		Nous sommes au sommet d'un chariot    \newline
		En chemin vers la ligne de front
		}
		}
		
	\end{verse}
	
	
	\begin{verse}
		No \textit{patiu} pas per mi, mare
		Faig lo que em vau ensenyar
		Serem les dones valentes
		Sense por del que vindrà
		\textnote{
			\useipafont{/No / pa.tiw / pas / pər / mi / ˈma.ɾə/}\newline
			\useipafont{/Fatʃ / lo / kə̆ / əm / vaw / ən.se.ˈɲaɾ/}\newline
			\useipafont{/ˈSe.ɾɛm / las / do.nəs / va.len.təs/}\newline
			\useipafont{/Sən.se / puɾ / dəl / ke / vin.ˈdɾa/}
		}
		\textnote{
		\textit{\small
		Ne pleure pas pour moi, mère    \newline
		Je fais ce que tu m'as appris     \newline
		Nous serons des femmes courageuses     \newline
		Sans peur de ce qui va arriver
		}
		}
		
		%		\sfH L\sfLong{a} mia t\sfLong{e}st\sfLong{a} schiacci\sfLong{a}te\sfDouble{-e} la p\sfLong{u}\sfDouble{-u-}re
		%		\sfH Di\textit{sse}  Cas\sfLong{e}rio aglinqu\sfLong{i}si\sfDouble{-ei-}si suo\sfDouble{-o-}i   
		%		\sfH Ma l’a\sfV narchi\sfV a è più fo\sfV rte\sfDouble{-e} de tu\sfLong{o}\sfDouble{-o-}i
		%		Pr\sfLong{e}sto pr\sfLong{e}sto\sfDouble{-o} shiacci\sfLong{a}rvi\sfDouble{-i} dovrà.
	\end{verse}
	
	\begin{verse}
		Si Madrid cau la primera
		Aragó també caurà
		Si Aragó cau presonera
		Nosaltres caurem demá
		Si no la lluitem nosaltres
		Ningú més la lluitarà
		\textnote{
			\useipafont{/Si / Ma.dɾid / kaw / la / pɾi.ˈme.ɾa/}\newline
			\useipafont{/A.ɾa.ˌɡo / tam.ˌbe / kaw.ˈɾa/}\newline
			\useipafont{/Sĭ / A.ɾa.ˈɡo / kaw / pɾe.zo.ne.ɾa/}\newline
			\useipafont{/No.zal.ˈtɾes / kaw.ɾɛm / dəma/}\newline
			\useipafont{/Si / no / la / ʎuj.tɛm / no.ˈzal.tɾes/}\newline
			\useipafont{/Niŋ.ˌɡu / mes / la / ʎuj.taˈ.ɾa/}
		}
		\textnote{
		\textit{\small
		Si Madrid tombe en premier     \newline
		L'Aragon tombera également     \newline
		Si Aragon tombe prisonnier     \newline
		Nous tomberons demain    \newline
		Si nous ne le combattons pas  \newline
		Personne d'autre ne le combattra
		}
		}
	\end{verse}

\begin{chorus}
La la la la.. x4
\end{chorus}
\textnote{Plus lent}

\begin{verse*}
	Si no la lluitem nosaltres
	Ningú més la lluitarà
	\end{verse*}
	
}

\endsong